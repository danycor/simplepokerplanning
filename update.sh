#!/bin/bash

# Init
version=$1

if [[ -z "$1" ]]; then
   echo "Version is not defined"
   exit 1
fi

echo "Start update add to $version" >> update-log.txt

# Backup configuration
mv ./api-server/.env api-server/.env.back

# Git pull server

git fetch --all
git checkout $version

# Restore configuration
mv ./api-server/.env.back api-server/.env

# Docker compose down up
docker-compose down
docker-compose up -d