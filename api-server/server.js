import express from "express";
import swaggerUi from "swagger-ui-express";
import bodyparser from "body-parser";
import "dotenv/config";
import cors from "cors";
import expressWs from "express-ws";

import swaggerConfiguration from "./src/swagger-output.json" assert { type: "json" };
import { router as TableRouter } from "./src/endpoints/TableEndpoints.js";
import { router as SecurityRouter } from "./src/endpoints/SecurityEndpoints.js";

import SocketManager from "./src/WebSocket.js";
import Database from "./src/Database.js";
import { CleanTableCron } from "./src/Cron.js";
import Logger from "./src/Logger.js";
import ApiException from "./src/ApiException.js";

const app = express();
expressWs(app);

let shutting_down = false;

if (process.env.NODE_ENV != "production") {
  Logger.warn("Cors started");
  app.use(cors());
}

app.use(bodyparser.json());
app.use((req, resp, next) => {
  if (!shutting_down) {
    return next();
  }
  resp.setHeader("Connection", "close");
  throw new ApiException(
    503,
    "503-001",
    "Server is in the process of restarting"
  );
});

app.use(Logger.middleware);

app.use("/api/auth", SecurityRouter);
app.use("/api", TableRouter);

// Websocket
console.log(SocketManager.processEndpoint);
app.ws("/api/ws", (ws) => {
  SocketManager.processEndpoint(ws);
});

// At the end, add swagger
app.use("/api/", swaggerUi.serve, swaggerUi.setup(swaggerConfiguration));

app.use((err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }
  if (err instanceof ApiException) {
    res.status(err.status).send({ code: err.code, message: err.message });
  } else {
    next(err);
  }
});

// Start server
const server = app.listen(process.env.port, () =>
  Logger.info("API started at " + process.env.url + ":" + process.env.port)
);

// Crons
CleanTableCron.register();

// Shut down
const cleanup = () => {
  shutting_down = true;
  server.close(() => {
    Database.stop();
    process.exit();
  });

  SocketManager.closeConnexions();

  setTimeout(() => {
    Logger.error("Could not close connections in time, forcing shut down");
    process.exit(1);
  }, 10 * 1000);
};

process.on("SIGINT", cleanup);
process.on("SIGTERM", cleanup);
