import { schedule as CronSchedule } from "node-cron";

import {deleteTable} from "./services/TableService.js";
import Logger from "./Logger.js";


const CLEAN_TABLE_CRON = "* */6 * * *";

class CleanTable {
  cleanTables = async () => {
    let tables = await TableService.getAll();
    let maxLife = 12 * 60 * 60; // 12H
    let currentDateTime = Math.floor(Date.now() / 1000);
    tables.forEach(async (table) => {
      if (table.creationDateTime + maxLife > currentDateTime) {
        return;
      }
      await deleteTable(table.code, { uuid: table.owner.id });
      Logger.info("Table ", table.code, "  cleaned");
    });
  };

  register = () => {
    CronSchedule(CLEAN_TABLE_CRON, () => {
      conLogger.info("Start cleanup");
      this.cleanTables();
      Logger.info("End cleanup");
    });
  };
}

export const CleanTableCron = new CleanTable();
