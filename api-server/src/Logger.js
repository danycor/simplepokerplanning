import Axe from "axe";
import Cabin from "cabin";
import Signale from "signale";

const logger = new Axe({
  logger: Signale,
  omittedFields: [ 'app' ]
});
const cabin = new Cabin({ logger });

export default cabin;
