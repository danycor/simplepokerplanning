import swaggerAutogen from "swagger-autogen";
import "dotenv/config";

const doc = {
  info: {
    title: "PokerPlanning Express API",
    version: "0.1.0",
    description: "Provide interfaces to manager poker planning",
    license: {
      name: "MIT",
      url: "https://spdx.org/licenses/MIT.html",
    },
    contact: {
      name: "CORBINEAU Dany",
      url: "https://dany-corbineau.fr",
    },
  },
  host: process.env.SWAGGER_HOST,
  schemes: ["http"],
  securityDefinitions: {
    securityToken: {
      type: 'apiKey',
      in: 'header', 
      name: 'authorization',
      description: 'Bearer token. Exemple : TOKEN VALUE.OFMY.TOKEN'
    }
  }
};

const outputFile = "./swagger-output.json";
const routes = ["../server.js"];

/* NOTE: If you are using the express Router, you must pass in the 'routes' only the 
root file where the route starts, such as index.js, app.js, routes.js, etc ... */

swaggerAutogen(outputFile, routes, doc);
