import express from "express";

const router = express.Router();
import SecurityService from "../services/SecurityService.js";
import ApiException from "../ApiException.js";

router.post("/login", (request, response, next) => {
  /*  #swagger.parameters['body'] = {
            in: 'body',
            description: 'Authentification data',
            schema: {
                $username: 'IamTheUser',
            }
    } */

  /* #swagger.responses[200] = {
          description: 'Token value',
          schema: {
              token: 'value of the token'
          }
  } */

  const username = request.body?.username;
  if (username == null || username.trim() === "") {
    throw new ApiException(400, "400-2", "Invalid uername");
  }
  new Promise((resolve) =>
    SecurityService.needAuthenticateToken(request, response, resolve)
  ).then(() => {
    response.status(200).json({
      token:
        request.user == undefined
          ? SecurityService.login(username)
          : SecurityService.updateUsername(username, request.user.uuid),
    });
  });
});

export { router };
