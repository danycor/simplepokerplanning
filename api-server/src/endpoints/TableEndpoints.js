import express from "express";
const router = express.Router();

import SocketManager from "../WebSocket.js";
import {
  create,
  join,
  getAll,
  getTableByCode,
  deleteTable,
  changeOwner,
  banUser,
  unbanUser,
  vote,
  revealing,
  cleanRevealed,
} from "../services/TableService.js";
import SecurityService from "../services/SecurityService.js";
import ApiException from "../ApiException.js";

function sendNotification(players, message) {
  players.forEach((player) => {
    let connexion = SocketManager.connections.find(
      (connexion) => connexion.clientId == player.id
    );
    if (connexion != undefined) {
      connexion.socket.send(JSON.stringify(message));
    }
  });
}

function sendNotificationForPlayers(table, message) {
  sendNotification(table.players, message);
}

function sendNotificationForBanned(players, message) {
  sendNotification(players, message);
}

router.post(
  "/table",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    /*
      #swagger.security = [{
        "securityToken": []
      }]
    */
    create(request.user)
      .then((v) => {
        response.status(201).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);
router.get(
  "/tables",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    /*
      #swagger.security = [{
        "securityToken": []
      }]
    */
    getAll()
      .then((v) => {
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.get(
  "/table/:tableId",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    getTableByCode(request.params.tableId.toUpperCase(), request.user)
      .then((v) => {
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.delete(
  "/table/:tableId",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    deleteTable(request.params.tableId.toUpperCase(), request.user)
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "TableClose",
          content: "Table is close",
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.put(
  "/table/:tableId/join",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    join(request.params.tableId.toUpperCase(), request.user)
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "PlayerJoin",
          content: v,
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.put(
  "/table/:tableId/changeOwner/:newUserUuid",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    changeOwner(
      request.params.tableId.toUpperCase(),
      request.params.newUserUuid,
      request.user
    )
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "OwnerChange",
          content: v,
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.put(
  "/table/:tableId/ban/:userUuid",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    banUser(request.params.tableId.toUpperCase(), request.params.userUuid, request.user)
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "PlayerBan",
          content: v,
        });
        sendNotificationForBanned(v.excludesPlayers, {
          type: "PlayerIsBanned",
          content: "You have been banned",
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.put(
  "/table/:tableId/unban/:userUuid",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    unbanUser(request.params.tableId.toUpperCase(), request.params.userUuid, request.user)
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "PlayerUnban",
          content: v,
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.put(
  "/table/:tableId/vote/:voteValue",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    vote(request.params.tableId.toUpperCase(), request.params.voteValue, request.user)
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "PlayerVote",
          content: v,
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.put(
  "/table/:tableId/revealed",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    revealing(request.params.tableId.toUpperCase(), request.user)
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "TableRevelaning",
          content: v,
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

router.put(
  "/table/:tableId/cleanRevealed",
  SecurityService.needAuthenticateToken,
  (request, response, next) => {
    cleanRevealed(request.params.tableId.toUpperCase(), request.user)
      .then((v) => {
        sendNotificationForPlayers(v, {
          type: "TableClean",
          content: v,
        });
        response.status(200).json(v);
      })
      .catch((err) =>
        next(
          err instanceof ApiException
            ? err
            : new ApiException(500, "500-x", err)
        )
      );
  }
);

export { router };
