import sqlite3 from 'sqlite3';

sqlite3.verbose()
const db = new sqlite3.Database('data/database.db')

function stop() {
    db.close()
}

const Database = {
    db, stop
}
export default Database;