import SecurityService from "./services/SecurityService.js";

class SocketManager {
  constructor() {
    this.connections = [];
  }

  processEndpoint(ws) {
    this.connections.push({ clientId: null, socket: ws });

    ws.on("message", (message) => {
      let messageObject = JSON.parse(message);
      switch (messageObject.type) {
        case "AuthToken":
          SecurityService.verifyToken(messageObject.content)
            .then((v) => {
              let connexion = this.connections.find((v) => v.socket === ws);
              if (connexion != undefined) {
                connexion.clientId = v.uuid;
              }
            })
            .catch((e) => {});
          break;
      }
    });
    ws.on("close", () => {
      this.connections = this.connections.filter((v) => v.socket != ws);
    });
  }

  closeConnexions() {
    this.connections.forEach((v) => v.socket.close());
  }
}
const socketManager = new SocketManager();

export default socketManager;
