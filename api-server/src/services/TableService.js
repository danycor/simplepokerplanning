import { v4 as uuidv4 } from "uuid";
import Logger from "../Logger.js";

import {
  Table,
  getAll as TableGetAll,
  insert as TableInsert,
  getByCode as TableGetByCode,
  deleteTable as TableDelete,
  insertTablePlayer as TableInsertPlayer,
  changeOwner as TableUpdateOwner,
  removePlayer as TableRemovePlayer,
  insertExcludesPlayer as TableInsertExcludePlayer,
  deleteExcludesPlayer as TableRemoveExcludesPlayer,
  updatePlayerVote as TableUpdateVoteValue,
  updateRevealing as TableUpdateRevealing,
} from "../model/Table.js";
import ApiException from "../ApiException.js";

const authorizedVoteValues = [
  "-",
  "0.5",
  "1",
  "2",
  "3",
  "5",
  "8",
  "13",
  "21",
  "33",
  "∞",
  "?",
];

export const create = async (user) => {
  const currentDate = new Date();
  let increment = 0;
  let tableCode = "";
  let exist = false;
  do {
    increment++;

    tableCode = (
      currentDate.getHours().toString(16) +
      currentDate.getMinutes().toString(16) +
      currentDate.getSeconds().toString(16) +
      increment.toString(16)
    ).toUpperCase();

    let tableFund = await TableGetByCode(tableCode);
    exist = tableFund != null;
  } while (exist);
  var table = new Table(uuidv4(), tableCode, { id: user.uuid });
  TableInsert(table);
  await TableInsertPlayer(table, user.uuid);
  Logger.info(user.username + " create table " + table.code);
  return await TableGetByCode(tableCode);
};

export const join = async (tableCode, user) => {
  let table = await TableGetByCode(tableCode);
  if (table == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (table.excludesPlayers.find((player) => player.id == user.uuid) != null) {
    throw new ApiException(401, "401-3", "Unauthorize to join table");
  }
  if (table.players.find((player) => player.id == user.uuid) == null) {
    await TableInsertPlayer(table, user.uuid);
  }
  return await TableGetByCode(tableCode);
};

export const getAll = () => {
  return TableGetAll();
};

export const getTableByCode = async (tableCode, user) => {
  const table = await TableGetByCode(tableCode);
  if (table == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (table.excludesPlayers.find((p) => p.id === user.uuid)) {
    throw new ApiException(401, "401-4", "Unauthorize");
  }
  return table;
};

export const deleteTable = async (tableCode, user) => {
  let tableFund = await TableGetByCode(tableCode);
  if (tableFund.owner.id != user.uuid) {
    throw new ApiException(
      401,
      "401-5",
      "Unauthorize - You are not the owner of the table"
    );
  }
  if (tableFund == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }

  TableDelete(tableFund.id);
  return tableFund;
};

export const changeOwner = async (tableCode, newOwnerUuid, user) => {
  let tableFund = await TableGetByCode(tableCode);
  if (tableFund == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (
    user.uuid != tableFund.owner.id ||
    tableFund.players.find((player) => player.id == newOwnerUuid) == null
  ) {
    throw new ApiException(
      401,
      "401-5",
      "Unauthorize - You cannot change the owner"
    );
  }
  await TableUpdateOwner(tableFund, newOwnerUuid);
  return await TableGetByCode(tableCode);
};

export const banUser = async (tableCode, playerUuid, user) => {
  let tableFund = await TableGetByCode(tableCode);
  if (tableFund == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (
    user.uuid != tableFund.owner.id ||
    tableFund.players.find((player) => player.id == playerUuid) == null
  ) {
    throw new ApiException(401, "401-6", "Unable to ban player");
  }

  await TableRemovePlayer(tableFund, playerUuid);
  await TableInsertExcludePlayer(tableFund, playerUuid);
  return await TableGetByCode(tableCode);
};

export const unbanUser = async (tableCode, playerUuid, user) => {
  let tableFund = await TableGetByCode(tableCode);
  if (tableFund == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (
    user.uuid != tableFund.owner.id ||
    tableFund.excludesPlayers.find((player) => player.id == playerUuid) == null
  ) {
    throw new ApiException(401, "401-6", "Unable to unban player");
  }

  await TableRemoveExcludesPlayer(tableFund, playerUuid);
  await TableInsertPlayer(tableFund, playerUuid);
  return await TableGetByCode(tableCode);
};

export const vote = async (tableCode, voteValue, user) => {
  let tableFund = await TableGetByCode(tableCode);
  if (tableFund == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (
    tableFund.isrevealed != 0 ||
    tableFund.players.find((player) => player.id == user.uuid) == null
  ) {
    throw new ApiException(401, "401-7", "Unable to vote");
  }
  if (authorizedVoteValues.find((v) => v == voteValue) == null) {
    throw new ApiException(
      400,
      "400-1",
      "Unauthorized vote value. Use : " + authorizedVoteValues.join(", ")
    );
  }

  TableUpdateVoteValue(tableFund, user.uuid, voteValue == "-" ? "" : voteValue);
  return await TableGetByCode(tableCode);
};

export const revealing = async (tableCode, user) => {
  let tableFund = await TableGetByCode(tableCode);
  if (tableFund == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (tableFund.isrevealed != 0 || tableFund.owner.id != user.uuid) {
    throw new ApiException(401, "401-8", "Unable to revealed");
  }
  await TableUpdateRevealing(tableFund, 1);
  return await TableGetByCode(tableCode);
};

export const cleanRevealed = async (tableCode, user) => {
  let tableFund = await TableGetByCode(tableCode);
  if (tableFund == null) {
    throw new ApiException(404, "404-1", "Unexisting table");
  }
  if (tableFund.isrevealed == 0 || tableFund.owner.id != user.uuid) {
    throw new ApiException(401, "401-9", "Unable to clean");
  }
  await TableUpdateRevealing(tableFund, 0);
  // set votes to ""
  return await TableGetByCode(tableCode);
};
