import { v4 as uuidv4 } from "uuid";

import { User, insert as UserInsert, update as UserUpdate } from "../model/User.js";
import jsonwebtoken from "jsonwebtoken";
import ApiException from "../ApiException.js";

class SercurityService {
  verifyToken = (authHeader) => {
    const token = authHeader && authHeader.split(" ")[1];
    if (token == null) {
      throw new ApiException(
        403,
        "403-1",
        "No token, please provide header:authorization"
      );
    }

    return new Promise((resolve, reject) => {
      jsonwebtoken.verify(token, process.env.JWT_KEY, (err, user) => {
        if (err) {
          reject(err);
        }
        resolve(user);
      });
    });
  };

  needAuthenticateToken = (req, resp, next) => {
    this.verifyToken(req.headers["authorization"])
      .then((user) => {
        if (user == null) {
          next(new ApiException(401, "401-1", "Unexisting user"));
        }
        req.user = user;
        next();
      })
      .catch((err) => {
        next(new ApiException(401, "401-2", err));
      });
  };

  login = (username) => {
    const uuid = uuidv4();
    const token = jsonwebtoken.sign(
      { uuid: uuid, username: username },
      process.env.JWT_KEY,
      { expiresIn: "3h" }
    );
    UserInsert(new User(uuid, username));
    return token;
  };

  updateUsername = (username, uuid) => {
    const token = jsonwebtoken.sign(
      { uuid: uuid, username: username },
      process.env.JWT_KEY,
      { expiresIn: "3h" }
    );
    UserUpdate(new User(uuid, username))
    return token;
  }

}

export default new SercurityService();
