import Database from "../Database.js";

export const tableName = 'users'

Database.db.run(`
CREATE TABLE IF NOT EXISTS `+tableName+` (
    id VARCHAR NOT NULL PRIMARY KEY,
    username VARCHAR
);
`);

export function insert(user) {
    const stmt = Database.db.prepare(
        `INSERT INTO `+tableName+` VALUES (?,?);`
    )
    stmt.run(user.id, user.username);
    stmt.finalize();
}

export function update(user) {
  const stmt = Database.db.prepare(
      "UPDATE " + tableName + " SET username=? WHERE id=?;",
  )
  stmt.run(user.username, user.id);
  stmt.finalize();
}

export async function getById(userId) {
  return await new Promise((resolve, reject) => {
    Database.db.get(
      "SELECT id, username FROM " +
        tableName +
        " WHERE id = ?;",
        userId,
      (error, row) => {
        if (error) {
          return reject(error);
        }
        if (row == null || row == undefined) {
          return resolve(null);
        }
        resolve(new User(row.id, row.username));
      }
    );
  });
}

export class User {
  constructor(id, username) {
    this.id = id;
    this.username = username;
  }
}
