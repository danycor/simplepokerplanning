import Database from "../Database.js";
import { getById as UserGetById } from "./User.js";

const tableName = "tables";
const tableNameTablePlayers = "tables_players";
const tableNameTableExcludedPlayers = "tables_excluded_players";

Database.db.run(
  `
CREATE TABLE IF NOT EXISTS ` +
    tableName +
    ` (
    id VARCHAR NOT NULL PRIMARY KEY,
    code VARCHAR,
    ownerId VARCHAR,
    creationDateTime INTEGER,
    isRevealed INTEGER
);
`
);

Database.db.run(
  `
CREATE TABLE IF NOT EXISTS ` +
    tableNameTablePlayers +
    ` (
    table_id VARCHAR,
    player_id VARCHAR,
    vote VARCHAR
);
`
);

Database.db.run(
  `
CREATE TABLE IF NOT EXISTS ` +
    tableNameTableExcludedPlayers +
    ` (
    table_id VARCHAR,
    player_id VARCHAR
);
`
);

export function insert(table) {
  const stmt = Database.db.prepare(
    `INSERT INTO ` + tableName + ` VALUES (?,?,?,?,?);`
  );
  table.creationDateTime = Math.floor(Date.now() / 1000);
  stmt.run(table.id, table.code, table.owner.id, table.creationDateTime, table.isrevealed);
  stmt.finalize();
}

export async function getAll() {
  return Promise.all(
    await new Promise((resolve, reject) => {
      Database.db.all(
        "SELECT id, code, ownerId, creationDateTime FROM " + tableName,
        (err, rows) => {
          if (err) {
            return reject(err);
          }
          let tablesPromises = [];
          rows.forEach(async (row) => {
            tablesPromises.push(getByCode(row.code));
          });
          resolve(tablesPromises);
        }
      );
    })
  );
}

export async function getByCode(code) {
  var table = await new Promise((resolve, reject) => {
    Database.db.get(
      "SELECT id, code, ownerId, creationDateTime, isRevealed FROM " +
        tableName +
        " WHERE code = ?;",
      code,
      (error, row) => {
        if (error) {
          return reject(error);
        }
        if (row == null || row == undefined) {
          return resolve(null);
        }
        UserGetById(row.ownerId)
          .then((v) => {
            var table = new Table(row.id, row.code, v);
            table.creationDateTime = row.creationDateTime;
            table.isrevealed = row.isRevealed;
            resolve(table);
          })
          .catch((e) => reject(e));
      }
    );
  });
  if (table == null) {
    return table;
  }
  table.players = await Promise.all(
    await new Promise((resolve, reject) => {
      Database.db.all(
        "SELECT table_id, player_id, vote FROM " +
          tableNameTablePlayers +
          " WHERE table_id = ?;",
        table.id,
        (error, rows) => {
          if (error) {
            return reject(error);
          }
          let playersPromises = [];
          rows.forEach((row) => {
            playersPromises.push(
              new Promise((res, rej) => {
                UserGetById(row.player_id)
                  .then((v) => {
                    v.vote = row.vote
                    res(v);
                  })
                  .catch((e) => rej(e));
              })
            );
          });
          resolve(playersPromises);
        }
      );
    })
  );
  // add excludes players
  table.excludesPlayers = await Promise.all(
    await new Promise((resolve, reject) => {
      Database.db.all(
        "SELECT table_id, player_id FROM " +
          tableNameTableExcludedPlayers +
          " WHERE table_id = ?;",
        table.id,
        (error, rows) => {
          if (error) {
            return reject(error);
          }
          let playersPromises = [];
          rows.forEach((row) => {
            playersPromises.push(UserGetById(row.player_id));
          });
          resolve(playersPromises);
        }
      );
    })
  );
  return table;
}

export async function deleteTable(id) {
  await new Promise((resolve, reject) => {
    Database.db.run(
      "DELETE FROM " + tableNameTablePlayers + " WHERE table_id = ?;",
      id,
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
  await new Promise((resolve, reject) => {
    Database.db.run(
      "DELETE FROM " + tableNameTableExcludedPlayers + " WHERE table_id = ?;",
      id,
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
  return new Promise((resolve, reject) => {
    Database.db.run(
      "DELETE FROM " + tableName + " WHERE id = ?;",
      id,
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
}

export async function insertTablePlayer(table, playerId) {
  const stmt = Database.db.prepare(
    `INSERT INTO ` + tableNameTablePlayers + ` VALUES (?,?, "");`
  );
  stmt.run(table.id, playerId);
  stmt.finalize();

  return getByCode(table.code);
}

export async function changeOwner(table, newOwnerId) {
  return await new Promise((resolve, reject) => {
    Database.db.run(
      "UPDATE " + tableName + " SET ownerId=? WHERE id=?;",
      [newOwnerId, table.id],
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
}

export async function removePlayer(table, playerId) {
  return new Promise((resolve, reject) => {
    Database.db.run(
      "DELETE FROM " +
        tableNameTablePlayers +
        " WHERE table_id = ? and player_id = ?;",
      [table.id, playerId],
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
}

export async function insertExcludesPlayer(table, playerId) {
  const stmt = Database.db.prepare(
    `INSERT INTO ` + tableNameTableExcludedPlayers + ` VALUES (?,?);`
  );
  stmt.run(table.id, playerId);
  stmt.finalize();
}

export async function deleteExcludesPlayer(table, playerId) {
  return new Promise((resolve, reject) => {
    Database.db.run(
      "DELETE FROM " +
        tableNameTableExcludedPlayers +
        " WHERE table_id = ? and player_id = ?;",
      [table.id, playerId],
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
}

export async function updatePlayerVote(table, playerId, voteValue) {
  return new Promise((resolve, reject) => {
    Database.db.run(
      "UPDATE  " +
        tableNameTablePlayers +
        " SET vote = ? WHERE table_id = ? and player_id = ?;",
      [voteValue, table.id, playerId],
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
}

export async function updateRevealing(table, value) {
  return new Promise((resolve, reject) => {
    Database.db.run(
      "UPDATE " +
        tableName +
        " SET isRevealed = ? WHERE id = ?;",
      [value, table.id],
      (error) => {
        if (error) {
          return reject(error);
        }
        return resolve();
      }
    );
  });
}

export class Table {
  constructor(id, code, owner) {
    this.id = id;
    this.code = code;
    this.owner = owner;
    this.players = [];
    this.excludesPlayers = [];
    this.creationDateTime = 0;
    this.isrevealed = 0;
  }
}
