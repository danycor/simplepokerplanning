import { Fragment, useCallback, useState, useEffect } from "react";
import { useTheme } from "@mui/material/styles";
import {
  Divider,
  Drawer,
  List,
  ListItemButton,
  ListItem,
  ListItemIcon,
  ListItemText,
  Tooltip,
  Typography,
  IconButton,
} from "@mui/material";
import StarIcon from "@mui/icons-material/Star";
import FaceRetouchingNaturalIcon from "@mui/icons-material/FaceRetouchingNatural";
import AccessibilityIcon from "@mui/icons-material/Accessibility";
import PersonOffIcon from "@mui/icons-material/PersonOff";
import SwitchAccessShortcutIcon from "@mui/icons-material/SwitchAccessShortcut";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import { useSnackbar } from "notistack";
import { useTranslation } from "react-i18next";

import HelpOutlineIcon from "@mui/icons-material/HelpOutline";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import Style from "../constants/Style";

import {
  getUserId as ApiGetUserId,
  banPlayer as ApiBanPlayer,
  unbanPlayer as ApiUnbanPlayer,
  changeOwner as ApiChangeOwner,
} from "../provider/api";

export default function TableUsersDrawer(props) {
  const theme = useTheme();
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  const drawerWidth = Style.drawerWidth;

  const handleDrawerClose = () => {
    props.setOpen(false);
  };

  const mapPlayerList = useCallback(() => {
    return props.table.players.map((player) => {
      return {
        id: player.id,
        name: player.username,
        haveChoose: player.vote !== "",
        isOwner: player.id === props.table.owner.id,
        isYou: player.id === ApiGetUserId(),
      };
    });
  }, [props.table]);

  const [playerList, setPlayerList] = useState(mapPlayerList());

  useEffect(() => {
    setPlayerList(mapPlayerList());
  }, [mapPlayerList]);

  const ejectPlayer = (player) => {
    ApiBanPlayer(props.table.code, player.id)
      .then(() => {
        props.updateTableData();
        enqueueSnackbar(t("table_users_drawer_player_ejected") + player.name, {
          variant: "warning",
        });
      })
      .catch((e) => {
        enqueueSnackbar(
          t("table_users_drawer_error_when_ban_player") + e.message,
          {
            variant: "error",
          }
        );
      });
  };

  const unbanPlayer = (player) => {
    ApiUnbanPlayer(props.table.code, player.id)
      .then(() => {
        props.updateTableData();
        enqueueSnackbar(t("table_users_drawer_player_unbanned"), {
          variant: "warning",
        });
      })
      .catch((e) => {
        enqueueSnackbar(
          t("table_users_drawer_error_when_unban_player") + e.message,
          {
            variant: "error",
          }
        );
      });
  };

  const assignOwner = (player) => {
    ApiChangeOwner(props.table.code, player.id)
      .then(() => {
        props.updateTableData();
        enqueueSnackbar(t("table_users_drawer_assign_owner_to") + player.name, {
          variant: "info",
        });
      })
      .catch((e) => {
        enqueueSnackbar(
          t("table_users_drawer_error_when_assign_owner") + e.message,
          {
            variant: "error",
          }
        );
      });
  };

  const iAmOwner = () =>
    playerList.filter((p) => p.isOwner && p.isYou).length > 0;

  const getPlayerVoteStatus = (haveChoose) => {
    return haveChoose ? (
      <CheckCircleIcon color="success" />
    ) : (
      <HelpOutlineIcon color="primary" />
    );
  };

  return (
    <Fragment>
      <Drawer
        sx={{
          width: drawerWidth,
          flexShrink: 0,
          "& .MuiDrawer-paper": {
            width: drawerWidth,
            boxSizing: "border-box",
          },
        }}
        variant="persistent"
        anchor="right"
        open={props.open}
      >
        <props.drawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronLeftIcon />
            ) : (
              <ChevronRightIcon />
            )}
          </IconButton>
        </props.drawerHeader>
        <Divider />
        <Typography variant="h6" gutterBottom>
          {t("table_users_drawer_owner")}
        </Typography>
        <List>
          {playerList
            .filter((p) => p.isOwner)
            .map((player) => (
              <ListItem key={player.id} disablePadding>
                <ListItemIcon style={{ margin: "8px" }}>
                  {iAmOwner() && (
                    <Tooltip title={t("table_users_drawer_owner")}>
                      <StarIcon color="warning" />
                    </Tooltip>
                  )}
                  {!iAmOwner() && (
                    <Tooltip title={t("table_users_drawer_owner")}>
                      <StarIcon />
                    </Tooltip>
                  )}
                  <Tooltip title={t("table_users_drawer_state")}>
                    {getPlayerVoteStatus(player.haveChoose)}
                  </Tooltip>
                </ListItemIcon>
                <ListItemText
                  primary={player.name}
                  style={{ maxWidth: "6em" }}
                />
              </ListItem>
            ))}
        </List>
        <Divider />
        <Typography variant="h6" gutterBottom>
          {t("table_users_drawer_players")}
        </Typography>
        <List>
          {playerList
            .filter((p) => !p.isOwner && p.isYou)
            .map((player) => (
              <ListItem key={player.id} disablePadding>
                <ListItemIcon style={{ margin: "8px" }}>
                  <Tooltip title={t("table_users_drawer_you")}>
                    <FaceRetouchingNaturalIcon color="warning" />
                  </Tooltip>
                  <Tooltip title={t("table_users_drawer_state")}>
                    {getPlayerVoteStatus(player.haveChoose)}
                  </Tooltip>
                </ListItemIcon>

                <ListItemText
                  primary={player.name}
                  style={{ maxWidth: "6em" }}
                />
                {iAmOwner() && (
                  <ListItemButton>
                    <Tooltip title={t("table_users_drawer_eject")}>
                      <IconButton aria-label={t("table_users_drawer_eject")}>
                        <PersonOffIcon color="error" />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={t("table_users_drawer_assign_owner")}>
                      <IconButton
                        aria-label={t("table_users_drawer_assign_owner")}
                      >
                        <SwitchAccessShortcutIcon color="warning" />
                      </IconButton>
                    </Tooltip>
                  </ListItemButton>
                )}
              </ListItem>
            ))}
        </List>
        <Divider />
        <List>
          {playerList
            .filter((p) => !p.isOwner && !p.isYou)
            .map((player) => (
              <ListItem key={player.id} disablePadding>
                <ListItemIcon style={{ margin: "8px" }}>
                  <AccessibilityIcon />
                  <Tooltip title={t("table_users_drawer_state")}>
                    {getPlayerVoteStatus(player.haveChoose)}
                  </Tooltip>
                </ListItemIcon>

                <ListItemText
                  primary={player.name}
                  style={{ maxWidth: "6em" }}
                />

                {iAmOwner() && (
                  <ListItemButton>
                    <Tooltip title={t("table_users_drawer_eject")}>
                      <IconButton
                        aria-label={t("table_users_drawer_eject")}
                        onClick={() => ejectPlayer(player)}
                      >
                        <PersonOffIcon color="error" />
                      </IconButton>
                    </Tooltip>
                    <Tooltip title={t("table_users_drawer_assign_owner")}>
                      <IconButton
                        aria-label={t("table_users_drawer_assign_owner")}
                        onClick={() => assignOwner(player)}
                      >
                        <SwitchAccessShortcutIcon color="warning" />
                      </IconButton>
                    </Tooltip>
                  </ListItemButton>
                )}
              </ListItem>
            ))}
        </List>
        <Divider />
        <Typography variant="h6" gutterBottom>
          {t("table_users_drawer_banned_players")}
        </Typography>
        <List>
          {props.table.excludesPlayers.map((player) => (
            <ListItem key={player.id} disablePadding>
              <ListItemText
                primary={player.username}
                style={{ maxWidth: "6em" }}
              />
              {iAmOwner() && (
                <ListItemButton>
                  <Tooltip title={t("table_users_drawer_unban")}>
                    <IconButton
                      aria-label={t("table_users_drawer_unban")}
                      onClick={() => unbanPlayer(player)}
                    >
                      <PersonOffIcon color="warning" />
                    </IconButton>
                  </Tooltip>
                </ListItemButton>
              )}
            </ListItem>
          ))}
        </List>
      </Drawer>
    </Fragment>
  );
}
