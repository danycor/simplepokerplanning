import * as React from "react";
import {Button, DialogActions, DialogTitle, DialogContentText, Dialog, DialogContent} from '@mui/material';
import { useTranslation } from "react-i18next";

export default function WarningDialog(props) {
  const { t } = useTranslation();
  const handleClose = () => {
    props.closeCallback();
  };

  const handleValidate = () => {
    props.validateCallback();
    props.closeCallback();
  }

  return (
    <div>
      <Dialog
        open={props.open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          {props.question}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {props.description}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>{t("warning_dialog_close_abort")}</Button>
          <Button onClick={handleValidate} autoFocus>
            {t("warning_dialog_validate")}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
