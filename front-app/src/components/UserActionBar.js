import { Fragment, useState } from "react";
import { Card, Button, AppBar, Toolbar, Typography } from "@mui/material";
import Commonstyles from "./styles-common.module.css";
import { useSnackbar } from "notistack";
import { useTranslation } from "react-i18next";

import { getUserId as ApiGetUserId, vote as ApiVote } from "../provider/api";

export default function UserActionBar(props) {
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  const [selectionOptions, setSelectionOptions] = useState(() => {
    const playerChoice = props.table.players.find(
      (p) => p.id === ApiGetUserId()
    ).vote;
    const values = [
      { key: 1, value: "0.5", isSelected: false },
      { key: 2, value: "1", isSelected: false },
      { key: 3, value: "2", isSelected: false },
      { key: 4, value: "3", isSelected: false },
      { key: 5, value: "5", isSelected: false },
      { key: 6, value: "8", isSelected: false },
      { key: 7, value: "13", isSelected: false },
      { key: 8, value: "21", isSelected: false },
      { key: 9, value: "33", isSelected: false },
      { key: 10, value: "∞", isSelected: false },
      { key: 11, value: "?", isSelected: false },
    ];
    return values.map((v) => {
      v.isSelected = v.value === playerChoice;
      return v;
    });
  });
  const selectCard = (cardElement) => {
    if (props.gameState !== "revealed") {
      setSelectionOptions(
        selectionOptions.map((option) => {
          option.isSelected =
            option.key === cardElement.key && !option.isSelected;
          return option;
        })
      );
      let selection = selectionOptions.find((v) => v.isSelected);
      ApiVote(props.table.code, selection != null ? selection.value : "-")
        .then(() => {
          props.updateTableData();
          if (selection != null) {
            enqueueSnackbar(
              t("user_action_bar_you_select_value") + selection.value,
              {
                variant: "info",
              }
            );
          } else {
            enqueueSnackbar(t("user_action_bar_you_unselect_value"), {
              variant: "info",
            });
          }
        })
        .catch((e) => {
          enqueueSnackbar(t("user_action_bar_error_when_vote") + e.message, {
            variant: "error",
          });
        });
    }
  };

  return (
    <Fragment>
      <AppBar position="static" color="primary" style={{ marginTop: "12px" }}>
        <Toolbar
          className={Commonstyles.action_tool_bar + Commonstyles.inline_list}
          style={{ flexWrap: "wrap" }}
        >
          <Typography variant="h6">
            {t("user_action_bar_user_actions")}
          </Typography>
          {selectionOptions.map((_, key) => (
            <Card
              key={key}
              className={Commonstyles.user_action_select_card}
              style={_.isSelected ? { backgroundColor: "green" } : {}}
            >
              <Button
                style={_.isSelected ? { color: "white" } : {}}
                onClick={() => selectCard(_)}
                variant="text"
              >
                {_.value}
              </Button>
            </Card>
          ))}
        </Toolbar>
      </AppBar>
    </Fragment>
  );
}
