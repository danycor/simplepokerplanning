import { useTranslation } from "react-i18next";
function NoPage () {
    const { t } = useTranslation();
    return (
        <h1>{t("nopage_message")}</h1>
    )
}

export default NoPage;