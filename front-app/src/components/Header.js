import { useState } from "react";
import MuiAppBar from "@mui/material/AppBar";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select from "@mui/material/Select";
import HomeIcon from "@mui/icons-material/Menu";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import Language from "../constants/Languages";
import Style from "../constants/Style";

function Header(props) {
  const [language, setLanguage] = useState(localStorage.getItem("language"));
  const { i18n, t } = useTranslation();

  const AppBar = styled(MuiAppBar, {
    shouldForwardProp: (prop) => prop !== "open",
  })(({ theme, open }) => ({
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    ...(open && {
      width: `calc(100% - ${Style.drawerWidth}px)`,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen,
      }),
      marginRight: Style.drawerWidth,
    }),
  }));

  let navigate = useNavigate();
  const navigateToHome = () => {
    navigate("/");
  };

  const handleLanguageChange = (event) => {
    setLanguage(event.target.value);
    localStorage.setItem("language", event.target.value);
    i18n.changeLanguage(event.target.value);
  };

  const handleDrawerOpen = () => {
    props.setOpen(true);
  };

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" open={props.open}>
        <Toolbar>
          <Typography
            onClick={navigateToHome}
            variant="h6"
            component="div"
            sx={{ flexGrow: 1 }}
            style={{ cursor: "pointer" }}
          >
            Poker Planning
          </Typography>
          <FormControl variant="filled">
            <InputLabel id="language-select-label">
              {t("header_language")}
            </InputLabel>
            <Select
              labelId="language-select-label"
              id="language-select"
              value={language}
              label={t("header_language")}
              onChange={handleLanguageChange}
            >
              {Language.map(({ code, label }) => (
                <MenuItem key={code} value={code}>
                  {t("header_langue_" + label)}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <IconButton
            size="large"
            edge="start"
            color="inherit"
            aria-label="Home"
            sx={{ ...(props.open && { display: "none" }) }}
            onClick={handleDrawerOpen}
          >
            <HomeIcon />
          </IconButton>
        </Toolbar>
      </AppBar>
    </Box>
  );
}

export default Header;
