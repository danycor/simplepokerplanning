import { useEffect, useState, Fragment } from "react";
import { AppBar, Button, Toolbar, Typography } from "@mui/material";
import Commonstyles from "./styles-common.module.css";
import WarningDialog from "./WarningDialog";
import { useNavigate } from "react-router-dom";
import { useSnackbar } from "notistack";
import { useTranslation } from "react-i18next";

import {
  getUserId as ApiGetUserId,
  revealed as ApiRevealed,
  clean as ApiClean,
  closeTable as ApiCloseTable,
} from "../provider/api";

export default function TableActionBar(props) {
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();

  const [dialogOpen, setDialogOpen] = useState(false);
  let navigate = useNavigate();
  const countDownSpeed = 0.1;
  const [countDownValue, setCountDownValue] = useState(0);

  useEffect(() => {
    let countDownInterval;
    if (countDownValue > 0 && props.gameState === "revealing") {
      countDownInterval = setTimeout(() => {
        setCountDownValue(countDownValue - countDownSpeed);
        if (countDownValue <= countDownSpeed) {
          ApiRevealed(props.table.code)
            .then(() => {
              props.updateTableData();
              props.setGameState("revealed");
            })
            .catch((e) => {
              enqueueSnackbar(
                t("table_action_bar_error_when_revealing") + e.message,
                {
                  variant: "error",
                }
              );
            });
        }
      }, 1000 * countDownSpeed);
    }
    return () => clearTimeout(countDownInterval);
  });

  const cleanValues = () => {
    ApiClean(props.table.code)
      .then(() => {
        props.updateTableData();
        props.setGameState("chosing");
      })
      .catch((e) => {
        enqueueSnackbar(t("table_action_bar_error_when_clean") + e.message, {
          variant: "error",
        });
      });
  };

  const revealingCard = () => {
    if (props.gameState === "chosing") {
      props.setGameState("revealing");
      enqueueSnackbar(t("table_action_bar_will_revealing"), {
        variant: "warning",
      });
      setCountDownValue(3);
    } else if (props.gameState === "revealing") {
      enqueueSnackbar(t("table_action_bar_revealing_abort"), {
        variant: "warning",
      });
      props.setGameState("chosing");
    } else if (props.gameState === "revealed") {
      cleanValues();
    }
  };

  return (
    <Fragment>
      <h1>
        {t("table_action_table")} {props.table.code}
      </h1>
      {props.table.owner.id === ApiGetUserId() && (
        <AppBar position="static">
          <Toolbar className={Commonstyles.action_tool_bar}>
            <Typography variant="h6">
              {t("table_action_table_actions")}
            </Typography>
            <Button
              onClick={revealingCard}
              style={{ backgroundColor: "green", color: "white" }}
            >
              {props.gameState === "chosing"
                ? t("table_action_bar_reveal")
                : props.gameState === "revealing"
                ? t("table_action_bar_stop_reveal") +
                  " (" +
                  Math.ceil(countDownValue) +
                  ")"
                : t("table_action_bar_clean")}
            </Button>

            <Button
              variant="contained"
              color="error"
              onClick={() => {
                setDialogOpen(true);
              }}
            >
              {t("table_action_bar_close_table")}
            </Button>
            <WarningDialog
              title={t("table_action_bar_close_table")}
              question={t("table_action_bar_close_table_question")}
              description={t("table_action_bar_close_table_description")}
              validateCallback={() => {
                ApiCloseTable(props.table.code)
                  .then(() => {
                    enqueueSnackbar(t("table_action_bar_table_closed"), {
                      variant: "success",
                    });
                    navigate("/");
                  })
                  .catch((e) => {
                    enqueueSnackbar(
                      t("table_action_bar_error_when_closing") + e.message,
                      {
                        variant: "error",
                      }
                    );
                  });
              }}
              closeCallback={() => setDialogOpen(false)}
              open={dialogOpen}
            />
          </Toolbar>
        </AppBar>
      )}
    </Fragment>
  );
}
