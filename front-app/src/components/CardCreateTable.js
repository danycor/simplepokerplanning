import AddCircle from "@mui/icons-material/AddCircle";
import { useState } from "react";
import useWebSocket from "react-use-websocket";
import { useTranslation } from "react-i18next";


import {
  Box,
  Button,
  Card,
  CardContent,
  Typography,
  TextField,
} from "@mui/material";
import {createTable as ApiCreateTable, getToken as ApiGetToken } from "../provider/api"
import { useSnackbar } from 'notistack';
import { useNavigate } from "react-router-dom";




export default function CardCreateTable() {
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();
  const {sendJsonMessage} = useWebSocket(process.env.REACT_APP_API_WEBSOCKET, {share: true})
  const [tableOwnerName, setTableOwnerName] = useState("");
  let navigate = useNavigate();

  const createSessionAction = () => {
    ApiCreateTable(tableOwnerName).then((v) => {
      sendJsonMessage({type: "AuthToken", content: "Token "+ApiGetToken() })

      enqueueSnackbar(t("create_table_notif_created")+v.code, {variant: 'success'})
      navigate('/table/'+v.code)
    }).catch((e) => {
      enqueueSnackbar(t("create_table_notif_error")+e.message, {variant: 'error'})
    })
  };

  return (
    <Card variant="outlined">
      <CardContent>
        <Typography variant="h6" gutterBottom>
          {t("create_table_create_new_table")}
        </Typography>
        <Box display="flex" alignItems="center">
          <TextField
            id="input-table-owner-name"
            label={t("create_table_table_owner")}
            variant="outlined"
            value={tableOwnerName}
            onChange={(e) => setTableOwnerName(e.target.value)}
          />
        </Box>
        <Button
          variant="contained"
          color="primary"
          onClick={createSessionAction}
          style={{ marginTop: "16px" }}
          endIcon={<AddCircle style={{ marginLeft: "8px" }} />}
        >
          {t("create_table_create")}
        </Button>
      </CardContent>
    </Card>
  );
}
