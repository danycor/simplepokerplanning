import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import { useState } from "react";
import {
  Box,
  Button,
  Card,
  CardContent,
  Typography,
  TextField,
} from "@mui/material";
import useWebSocket from "react-use-websocket";
import { useSnackbar } from "notistack";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import {
  joinTable as ApiJoinTable,
  getToken as ApiGetToken,
} from "../provider/api";

export default function CardJoinTable() {
  const { t } = useTranslation();

  const { enqueueSnackbar } = useSnackbar();
  const { sendJsonMessage } = useWebSocket(
    process.env.REACT_APP_API_WEBSOCKET,
    { share: true }
  );
  let navigate = useNavigate();

  const [tableUserName, setTableUserName] = useState("");
  const [tableCode, setTableCode] = useState("");

  const createSessionAction = () => {
    ApiJoinTable(tableUserName, tableCode)
      .then((v) => {
        sendJsonMessage({
          type: "AuthToken",
          content: "Token " + ApiGetToken(),
        });
        enqueueSnackbar(t("join_table_table_joined") + v.code, {
          variant: "success",
        });
        navigate("/table/" + v.code);
      })
      .catch((e) => {
        enqueueSnackbar(t("join_table_table_joined_error") + e.message, {
          variant: "error",
        });
      });
  };

  return (
    <Card variant="outlined">
      <CardContent>
        <Typography variant="h6" gutterBottom>
          {t("join_table_join_table")}
        </Typography>
        <Box display="flex" alignItems="center">
          <TextField
            id="input-table-owner-name"
            label={t("join_table_table_code")}
            value={tableCode}
            onChange={(e) => setTableCode(e.target.value)}
          />
        </Box>
        <Box display="flex" alignItems="center" style={{ marginTop: "16px" }}>
          <TextField
            id="input-table-owner-name"
            label={t("join_table_table_user_name")}
            variant="outlined"
            value={tableUserName}
            onChange={(e) => setTableUserName(e.target.value)}
          />
        </Box>
        <Button
          variant="contained"
          color="primary"
          onClick={createSessionAction}
          style={{ marginTop: "16px" }}
          endIcon={<ArrowForwardIcon style={{ marginLeft: "8px" }} />}
        >
          {t("join_table_join")}
        </Button>
      </CardContent>
    </Card>
  );
}
