import { Fragment, useState, useCallback, useEffect } from "react";
import { Container, Grid, Card, Typography } from "@mui/material";
import { useTranslation } from "react-i18next";

export default function CardTable(props) {
  const { t } = useTranslation();

  const mapPlayerCardList = useCallback(() => {
    return props.table.players
      .map((p) => (p.vote !== "" ? p.vote : null))
      .filter((v) => v != null);
  }, [props.table]);

  const [playerCardsList, setPlayerCardList] = useState(mapPlayerCardList);

  useEffect(() => {
    setPlayerCardList(mapPlayerCardList());
  }, [mapPlayerCardList]);

  const cardStyle = {
    textAlign: "center",
    width: "3em",
    height: "2em",
    border: "1px solid #ccc",
    borderRadius: "8px",
  };
  const resultCardStyle = {
    textAlign: "center",
    border: "1px solid #ccc",
    borderRadius: "8px",
    padding: "0px 8px",
    margin: "8px",
  };
  return (
    <Fragment>
      <Container maxWidth="lg" style={{ marginTop: "12px" }}>
        <Grid container spacing={2} justifyContent="center">
          {playerCardsList.map((_, index) => (
            <Grid item xs={4} sm={3} md={2} lg={1} key={index}>
              <Card variant="outlined" style={cardStyle}>
                <Typography variant="h6" gutterBottom>
                  {props.gameState === "revealed" ||
                  props.table.isrevealed === 1
                    ? _
                    : "*"}
                </Typography>
              </Card>
            </Grid>
          ))}
        </Grid>
        {(props.gameState === "revealed" || props.table.isrevealed === 1) && (
          <Card variant="outlined" style={resultCardStyle}>
            <Typography variant="h6" gutterBottom>
              {t("card_table_result")}{" "}
              {playerCardsList
                .map((v) => parseFloat(v))
                .reduce(
                  (accumulator, currentValue) =>
                    isNaN(currentValue)
                      ? accumulator
                      : accumulator + currentValue,
                  0
                ) /
                Math.max(playerCardsList.filter((v) => !isNaN(v)).length, 1)}
              {playerCardsList.filter((v) => isNaN(v)).length > 0 ? " + " : ""}
              {playerCardsList.filter((v) => isNaN(v)).join(" + ")}
            </Typography>
          </Card>
        )}
      </Container>
    </Fragment>
  );
}
