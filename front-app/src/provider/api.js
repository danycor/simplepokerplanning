import { jwtDecode } from "jwt-decode";

const url = process.env.REACT_APP_API_URL;
const endpoints = {
  login: "/auth/login",
  table: "/table",
  vote: "/vote",
  ban: "/ban",
  unban: "/unban",
  revealed: "/revealed",
  clean: "/cleanRevealed",
  changeOwner: "/changeOwner",
};
const methods = {
  GET: "GET",
  POST: "POST",
  PUT: "PUT",
  DELETE: "DELETE",
};

function validateExpiration(token) {
  return token != null && jwtDecode(token).exp * 1000 > new Date() ? token : null
}

function setToken(token) {
  localStorage.setItem("token", token);
}

export function getToken() {
  return validateExpiration(localStorage.getItem("token"));
}

export function getUserId() {
  return jwtDecode(getToken()).uuid;
}

function getUsername() {
  return jwtDecode(getToken()).username;
}

async function login(username) {
  const response = await fetch(url + endpoints.login, {
    method: methods.POST,
    headers: {
      "Content-Type": "application/json",
      authorization: "token " + getToken(),
    },
    body: JSON.stringify({ username: username }),
  });
  const token = await response.json();
  setToken(token.token);
}

async function callApi(method, endpoint, body) {
  if (getToken() == null) {
    throw new Error("Not authtentificated");
  }
  let response = null;
  switch (method) {
    case methods.POST:
    case methods.PUT:
      response = await fetch(url + endpoint, {
        method: method,
        headers: {
          "Content-Type": "application/json",
          authorization: "token " + getToken(),
        },
        body: JSON.stringify(body),
      });
      break;
    default:
      response = await fetch(url + endpoint, {
        method: method,
        headers: {
          "Content-Type": "application/json",
          authorization: "token " + getToken(),
        },
      });
  }
  if (response == null) {
    return null;
  } else if (response.status !== 200 && response.status !== 201) {
    throw new Error(
      response.status + " - " + JSON.stringify(await response.json())
    );
  }

  return await response.json();
}

export async function createTable(username) {
  if ((getToken() == null || getToken() === undefined || username !== getUsername()) && username !== "") {
    await login(username);
  }
  return await callApi(methods.POST, endpoints.table, {});
}

export async function joinTable(username, tableCode) {
  if ((getToken() == null || getToken() === undefined || username !== getUsername()) && username !== "") {
    await login(username);
  }
  return await callApi(
    methods.PUT,
    endpoints.table + "/" + tableCode + "/join",
    {}
  );
}

export async function getTable(tableCode) {
  return await callApi(methods.GET, endpoints.table + "/" + tableCode, {});
}

export async function vote(tableCode, voteValue) {
  return await callApi(
    methods.PUT,
    endpoints.table +
      "/" +
      tableCode +
      endpoints.vote +
      "/" +
      encodeURIComponent(voteValue),
    {}
  );
}

export async function banPlayer(tableCode, playerId) {
  return await callApi(
    methods.PUT,
    endpoints.table + "/" + tableCode + endpoints.ban + "/" + playerId,
    {}
  );
}

export async function unbanPlayer(tableCode, playerId) {
  return await callApi(
    methods.PUT,
    endpoints.table + "/" + tableCode + endpoints.unban + "/" + playerId,
    {}
  );
}

export async function revealed(tableCode) {
  return await callApi(
    methods.PUT,
    endpoints.table + "/" + tableCode + endpoints.revealed,
    {}
  );
}

export async function clean(tableCode) {
  return await callApi(
    methods.PUT,
    endpoints.table + "/" + tableCode + endpoints.clean,
    {}
  );
}

export async function changeOwner(tableCode, newOwnerId) {
  return await callApi(
    methods.PUT,
    endpoints.table +
      "/" +
      tableCode +
      endpoints.changeOwner +
      "/" +
      newOwnerId,
    {}
  );
}

export async function closeTable(tableCode) {
  return await callApi(methods.DELETE, endpoints.table + "/" + tableCode, {});
}
