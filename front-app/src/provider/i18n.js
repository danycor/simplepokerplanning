import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import EnRessource from "../constants/en_resources";
import FrRessource from "../constants/fr_resources";

i18n.use(initReactI18next).init({
  lng: localStorage.getItem("language"),
  fallbackLng: "en",
  interpolation: {
    escapeValue: false,
  },
  resources: {
    en: EnRessource,
    fr: FrRessource,
  },
});

export default i18n;
