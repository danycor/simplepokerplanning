import { BrowserRouter, Routes, Route } from "react-router-dom";
import useWebSocket from "react-use-websocket";
import { useState } from "react";
import { useSnackbar } from "notistack";
import { useTranslation } from "react-i18next";

import NoPage from "./components/NoPage";
import Home from "./pages/home/Home";
import Table from "./pages/table/Table";
import Header from "./components/Header";
import { getToken as ApiGetToken } from "./provider/api";

function App() {
  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();
  const [open, setOpen] = useState(true);

  const { sendJsonMessage } = useWebSocket(
    process.env.REACT_APP_API_WEBSOCKET,
    {
      share: true,
      onOpen: () => {
        enqueueSnackbar(t("app_websocket_sucess"), { variant: "success" });
        sendJsonMessage({
          type: "AuthToken",
          content: "Token " + ApiGetToken(),
        });
      },
      onError: () => {
        enqueueSnackbar(t("app_websocket_fail"), { variant: "error" });
      },
    }
  );

  return (
    <BrowserRouter>
      <Header open={open} setOpen={(v) => setOpen(v)}/>

      <Routes>
        <Route index element={<Home />} />
        <Route path="table/:tableId" element={<Table open={open} setOpen={(v) => setOpen(v)}/>} />
        <Route path="*" element={<NoPage />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
