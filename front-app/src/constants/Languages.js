const Language = [
  { label: "French", code: "fr" },
  { label: "English", code: "en" }
];

export default Language;
