/*
import { useTranslation } from "react-i18next";
const { t } = useTranslation();
*/

const resource = {
    translation: {
        app_websocket_sucess: "Websocket connection sucess",
        app_websocket_fail: "Fail to connect websocket",
        header_language: "Language",
        header_langue_French: "French",
        header_langue_English: "English",
        home_title: "Home",
        nopage_message: "Page not found",
        create_table_notif_created: "Table created : ",
        create_table_notif_error: "Error when creating table : ",
        create_table_create_new_table: "Create new table",
        create_table_table_owner: "Table Owner",
        create_table_create: "Create",
        join_table_table_joined: "Table joined : ",
        join_table_table_joined_error: "Error when join table : ",
        join_table_join_table: "Join table",
        join_table_table_code: "Table Code",
        join_table_table_user_name: "Table user name",
        join_table_join: "Join",
        warning_dialog_close_abort: "Close / Abort action",
        warning_dialog_validate: "Validate action",
        table_player_join_table: "Player join table",
        table_player_has_been_banned: "Player has been banned",
        table_player_has_been_unbaned: "Player has been unbaned",
        table_you_have_been_banned: "You have been banned from the table ",
        table_revealed: "Table revealed",
        table_cleaning: "Table cleaning",
        table_is_close: "Table is close ",
        table_error_when_retreving: "Error when retreving table : ",
        table_loading: "Loading",
        card_table_result: "Result :",
        
        table_action_bar_error_when_revealing: "Error when revealing : ",
        table_action_bar_error_when_clean: "Error when clean table : ",
        table_action_bar_will_revealing: "Will revealing values",
        table_action_bar_revealing_abort: "Revealing abort",
        table_action_bar_reveal: "Reveal",
        table_action_bar_stop_reveal: "Stop reveal",
        table_action_bar_clean: "Clean",
        table_action_table:"Table :",
        table_action_table_actions: "Table Actions",
        table_action_bar_close_table: "Close table",
        table_action_bar_close_table_question: "Close the table for all players ?",
        table_action_bar_close_table_description: "This will close the table and eject all players",
        table_action_bar_table_closed: "Table closed",
        table_action_bar_error_when_closing: "Error when closing table : ",

        user_action_bar_you_select_value: "You select value : ",
        user_action_bar_you_unselect_value: "You unselect value",
        user_action_bar_error_when_vote: "Error when vote : ",
        user_action_bar_user_actions: "User actions",

        table_users_drawer_player_ejected: "Player ejected ",
        table_users_drawer_error_when_ban_player: "Error when ban player : ",
        table_users_drawer_player_unbanned: "Player unbanned ",
        table_users_drawer_error_when_unban_player: "Error when unban player : ",
        table_users_drawer_assign_owner_to: "Assign owner to ",
        table_users_drawer_error_when_assign_owner: "Error when assign owner : ",
        table_users_drawer_owner: "Owner",
        table_users_drawer_state: "State",
        table_users_drawer_players: "Players",
        table_users_drawer_you: "You",
        table_users_drawer_eject: "Eject",
        table_users_drawer_assign_owner: "Assign owner",
        table_users_drawer_banned_players: "Banned players",
        table_users_drawer_unban: "Unban",
      }
      
}

export default resource