import { useState, useEffect, Fragment } from "react";
import { useParams } from "react-router-dom";
import useWebSocket from "react-use-websocket";
import { Container, Typography } from "@mui/material";
import { styled } from "@mui/material/styles";
import { useSnackbar } from "notistack";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";

import TableActionBar from "../../components/TableActionBar";
import CardTable from "../../components/CardTable";
import UserActionBar from "../../components/UserActionBar";
import TableUsersDrawer from "../../components/TableUsersDrawer";
import { getTable as ApiGetTableByCode } from "../../provider/api";

function Table(props) {
  const { tableId } = useParams();

  const { enqueueSnackbar } = useSnackbar();
  const { t } = useTranslation();
  let navigate = useNavigate();
  const [gameState, setGameState] = useState("chosing");
  const [tableData, setTableData] = useState(null);

  const { lastJsonMessage } = useWebSocket(
    process.env.REACT_APP_API_WEBSOCKET,
    { share: true }
  );

  const DrawerHeader = styled("div")(({ theme }) => ({
    display: "flex",
    alignItems: "center",
    padding: theme.spacing(0, 1),
    ...theme.mixins.toolbar,
    justifyContent: "flex-start",
  }));

  useEffect(() => {
    if (lastJsonMessage !== null) {
      switch (lastJsonMessage.type) {
        case "PlayerJoin":
          setTableData(lastJsonMessage.content);
          enqueueSnackbar(t("table_player_join_table"), {
            variant: "info",
          });
          break;
        case "PlayerBan":
          setTableData(lastJsonMessage.content);
          enqueueSnackbar(t("table_player_has_been_banned"), {
            variant: "info",
          });
          break;
        case "PlayerUnban":
          setTableData(lastJsonMessage.content);
          enqueueSnackbar(t("table_player_has_been_unbaned"), {
            variant: "info",
          });
          break;
        case "PlayerIsBanned":
          navigate("/");
          enqueueSnackbar(t("table_you_have_been_banned") + tableId, {
            variant: "error",
          });
          break;
        case "TableRevelaning":
          setTableData(lastJsonMessage.content);

          enqueueSnackbar(t("table_revealed"), {
            variant: "success",
          });
          break;
        case "TableClean":
          setTableData(lastJsonMessage.content);
          enqueueSnackbar(t("table_cleaning"), {
            variant: "success",
          });
          break;
        case "TableClose":
          navigate("/");
          enqueueSnackbar(t("table_is_close") + tableId, {
            variant: "warning",
          });
          break;
        case "PlayerVote":
        case "OwnerChange":
          setTableData(lastJsonMessage.content);
          break;
        default:
      }
    }
  }, [setTableData, lastJsonMessage, enqueueSnackbar, navigate, tableId, t]);

  useEffect(() => {
    if (
      tableData !== null &&
      tableData.isrevealed !== 0 &&
      gameState !== "revealing"
    ) {
      setGameState("revealed");
    } else if (gameState !== "revealing") {
      setGameState("chosing");
    }
  }, [tableData, gameState]);

  useEffect(() => {
    let mounted = true;
    ApiGetTableByCode(tableId)
      .then((table) => {
        if (mounted) {
          setTableData(table);
          if (table.isrevealed !== 0) {
            setGameState("revealed");
          }
        }
      })
      .catch((e) => {
        enqueueSnackbar(t("table_error_when_retreving") + e.message, {
          variant: "error",
        });
        navigate("/");
      });
    return () => (mounted = false);
  }, [tableId, navigate, enqueueSnackbar, t]);

  const updateTableData = () => {
    ApiGetTableByCode(tableId)
      .then((table) => {
        setTableData(table);
      })
      .catch((e) => {
        enqueueSnackbar(t("table_error_when_retreving") + e.message, {
          variant: "error",
        });
        navigate("/");
      });
  };

  return (
    <Fragment>
      {tableData != null && tableData !== undefined ? (
        <Fragment>
          <Container maxWidth="lg">
            <DrawerHeader />
            <TableActionBar
              gameState={gameState}
              setGameState={setGameState}
              table={tableData}
              updateTableData={updateTableData}
            />
            <CardTable gameState={gameState} table={tableData} />
            <UserActionBar
              gameState={gameState}
              table={tableData}
              updateTableData={updateTableData}
            />
          </Container>
          <TableUsersDrawer
            table={tableData}
            updateTableData={updateTableData}
            drawerHeader={DrawerHeader}
            open={props.open}
            setOpen={(v) => props.setOpen(v)}
          />
        </Fragment>
      ) : (
        <Typography>{t("table_loading")}</Typography>
      )}
    </Fragment>
  );
}

export default Table;
