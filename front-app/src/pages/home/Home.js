import { Fragment } from "react";
import { useTranslation } from "react-i18next";
import Container from "@mui/material/Container";
import CardCreateTable from "../../components/CardCreateTable";
import CardJoinTable from "../../components/CardJoinTable";
import Grid from "@mui/material/Grid";

function Home() {
  const { t } = useTranslation();
  return (
    <Fragment>
      <Container maxWidth="lg">
        <h1>{t("home_title")}</h1>
        <Grid container spacing={2} justifyContent="center" alignItems="center">
          <Grid item xs={12} sm={6} lg={3}>
            <CardCreateTable />
          </Grid>
          <Grid item xs={12} sm={6} lg={3}>
            <CardJoinTable />
          </Grid>
        </Grid>
      </Container>
    </Fragment>
  );
}

export default Home;
